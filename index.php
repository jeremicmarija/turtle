<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function did_cros_previous_path(array $steps){
    $x = 0;
    $y = 0;

    $dirX = 0;
    $dirY = 1;

    $doneSteps = [];

    $firstCrossing = 0;

    foreach ($steps as $stepCount => $step) {

        for ($currentStep = 0; $currentStep < $step; $currentStep++) {

            $x += $dirX;
            $y += $dirY;

            $key = "{$x}x{$y}";

            if (in_array($key, $doneSteps) && 0 === $firstCrossing) {

                $firstCrossing = $stepCount + 1;
            }

            $doneSteps[] = "{$x}x{$y}";
        }

        if (1 === $dirY && 0 === $dirX) {

            $dirY = 0;

            $dirX = 1;
        } else if (-1 === $dirY && 0 === $dirX) {

            $dirY = 0;

            $dirX = -1;
        } else if (1 === $dirX && 0 === $dirY) {

            $dirY = -1;

            $dirX = 0;
        } else if (-1 === $dirX && 0 === $dirY) {

            $dirY = 1;

            $dirX = 0;
        }
    }

    echo "Crossing on {$firstCrossing}<br />";

}

$A = [1, 3, 2, 5, 4, 4, 6, 3, 2];



did_cros_previous_path($A);
