# README #

This README would normally document whatever steps are necessary to get your application up and running.

The task is :
A string A is given, with index 0, consisting of N integers of positive numbers.
The turtle stands at the position (0, 0) and moves to the north. A [0] moves forward and then turns 90 degrees clockwise. Then A [1] moves forward and turns 90 degrees clockwise, and so on.

For example, for a series:

A = [1, 3, 2, 5, 4, 4, 6, 3, 2]

The turtle moves as follows:

(0.0) -> (0.1) first move, 1 step north
(0.1) -> (3.1) second move, 3 steps to the east
(3,1) -> (3, -1) the third move, 2 steps to the south
(3, -1) -> (-2, -1) the fourth move, 5 steps to the west
(-2, -1) -> (-2,3) Fifth move, 4 steps to the north
(-2.3) -> (2.3) the sixth move, 4 steps to the east
(2,3) -> (2, -3) seventh move, 6 steps to the south
(2, -3) -> (-1 -3) eighth move, 3 steps to the west
(-1, -3) -> (-1, -1) ninth move, 2 steps to the north

At the seventh and ninth moves of the turtle touches the road that has previously crossed, specifically:

in point (2.1) in the sixth move,
in point (2, -1) in the seventh move,
at the point (-1, -1) in the ninth move

Write a feature:

function did_cross_previous_path (A)

which, for the path set in row A, returns the number of the first move in which the turtle touches its already laid-back path, or 0 if it does not touch the way ahead. For example, for string A from the previous example, the function should return number 7.

Assume that:
- Possible number of elements in a range in range [1..100,000]
- Each element of A is of type integer in the range [1..1,000,000]

Information on the expected complexity of the algorithm:
- time complexity in the worst case O (n)
- spatial complexity in the worst case of O (1), not counting the input data